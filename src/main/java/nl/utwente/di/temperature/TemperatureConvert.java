package nl.utwente.di.temperature;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class TemperatureConvert extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature fahrenheit" +
//                Double.toString(quoter.getBookPrice(request.getParameter("isbn"))) +
                Double.toString(Integer.parseInt(request.getParameter("celsius")) * 1.8 + 32) +
                "</BODY></HTML>");
    }


}